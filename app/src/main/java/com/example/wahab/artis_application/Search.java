package com.example.wahab.artis_application;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

public class Search extends AppCompatActivity {
    TextView tvProgressLabel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ImageView btn = (ImageView) findViewById(R.id.going_back);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Spinner dropdown = (Spinner) findViewById(R.id.spinner_medium);

        String[] items = new String[]{ "Select medium type", "Medium", "Medium1", "Medium2", "Medium3", "Medium4"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);

        dropdown.setAdapter(adapter);

        Spinner dropdown1 = (Spinner) findViewById(R.id.spinner_art);

        String[] items1 = new String[]{ "Select movement art", "movement art", "movement art1", "movement art2", "movement art3", "movement art4"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items1);

        dropdown1.setAdapter(adapter1);

        Spinner dropdown2 = (Spinner) findViewById(R.id.spinner_location);

        String[] items2 = new String[]{ "Select country", "Afganistan", "Pakistan", "China", "Sout africa", "Japan"};

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items2);

        dropdown2.setAdapter(adapter2);


        SeekBar seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

        int progress = seekBar.getProgress();
         tvProgressLabel = findViewById(R.id.textView);
        tvProgressLabel.setText("USD " + progress);
    }
    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // updated continuously as the user slides the thumb
            tvProgressLabel.setText("USD " + progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // called when the user first touches the SeekBar
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // called after the user finishes moving the SeekBar
        }
    };
}
