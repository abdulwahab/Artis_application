package com.example.wahab.artis_application;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class Favorites extends Fragment {
    private List<fragment> cardList = new ArrayList<>();
    private RecyclerView recyclerView;
    private FragmentAdapter fAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_favorites_fragment);

        fAdapter = new FragmentAdapter(cardList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(fAdapter);

        prepareFragmentData_fav();

        return view;
    }
    private void prepareFragmentData_fav() {
        fragment list = new fragment(R.drawable.likes, R.drawable.myfavorites);
        cardList.add(list);

        list = new fragment(R.drawable.likes1, R.drawable.myfavorites);
        cardList.add(list);

        list = new fragment(R.drawable.homeimg, R.drawable.myfavorites);
        cardList.add(list);

        list = new fragment(R.drawable.likes, R.drawable.myfavorites);
        cardList.add(list);

        list = new fragment(R.drawable.likes1, R.drawable.myfavorites);
        cardList.add(list);

        list = new fragment(R.drawable.homeimg, R.drawable.myfavorites);
        cardList.add(list);

        list = new fragment(R.drawable.likes, R.drawable.myfavorites);
        cardList.add(list);


        fAdapter.notifyDataSetChanged();
    }


}
