package com.example.wahab.artis_application;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class pre_login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_login);

        Button log_in = (Button) findViewById(R.id.goto_login);
        log_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(pre_login.this, login.class);
                startActivity(variable);
//                finish();
            }
        });

        Button signup = (Button) findViewById(R.id.go_signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(pre_login.this,sign_up.class);
                startActivity(variable);
//                finish();
            }
        });

        Button home = (Button) findViewById(R.id.goto_home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(pre_login.this,homepage.class);
                startActivity(variable);
            }
        });
    }
}
