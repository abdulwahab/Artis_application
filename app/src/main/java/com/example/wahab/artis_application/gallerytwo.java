package com.example.wahab.artis_application;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class gallerytwo extends AppCompatActivity {
    ViewPager viewPager;
    ViewPagerAdapter adpater;
    boolean xyz = false;

    private final static int NUM_PAGES = 3;
    private List<ImageView> dots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallerytwo);
        mainPart();
        addDots();

        ImageView btn = (ImageView) findViewById(R.id.back_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView btn1 = (ImageView) findViewById(R.id.go_upload);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(gallerytwo.this, Upload_art.class);
                startActivity(variable);

            }
        });

        Button btn2 = (Button) findViewById(R.id.go_home);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(gallerytwo.this, pre_login.class);
                startActivity(variable);

            }
        });

        final ImageView jd = (ImageView) findViewById(R.id.like_dislike);
        jd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (xyz == false) {
                    jd.setImageResource(R.drawable.like);
                    xyz = true;
                } else {
                    jd.setImageResource(R.drawable.likeg);
                    xyz = false;
                }

            }
        });
    }

    public void mainPart() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        adpater = new ViewPagerAdapter(this);
        viewPager.setAdapter(adpater);
        viewPager.setOffscreenPageLimit(30000);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    public void addDots() {
        dots = new ArrayList<>();
        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.dots);

        for (int i = 0; i < NUM_PAGES; i++) {
            ImageView dot = new ImageView(this);
            dot.setImageDrawable(getResources().getDrawable(R.drawable.slide));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    50,
                    LinearLayout.LayoutParams.MATCH_PARENT
            );
            dotsLayout.addView(dot, params);

            dots.add(dot);
        }
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectDot(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void selectDot(int idx) {
        Resources res = getResources();
        for (int i = 0; i < NUM_PAGES; i++) {
            int drawableId = (i == idx) ? (R.drawable.slide_select) : (R.drawable.slide);
            Drawable drawable = res.getDrawable(drawableId);
            dots.get(i).setImageDrawable(drawable);
        }
    }
}
