package com.example.wahab.artis_application;

import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


public class My_artis extends AppCompatActivity {
    Button btn_like;
    Button btn_fav;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_artis);

        android.support.v4.app.FragmentTransaction fragmentTransaction;
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_like, new Like());
        fragmentTransaction.commitAllowingStateLoss();


        btn_like = (Button) findViewById(R.id.like_btn);
        btn_like.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                btn_like.setBackgroundResource(R.drawable.btn_roundconer);
                btn_like.setTextColor(Color.parseColor("#ffffff"));

                btn_fav.setBackgroundResource(R.drawable.btn_conerwhite);
                btn_fav.setTextColor(Color.parseColor("#000000"));

                android.support.v4.app.FragmentTransaction fragmentTransaction;
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_like, new Like());
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        btn_fav = (Button) findViewById(R.id.fav_btn);
        btn_fav.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                btn_fav.setBackgroundResource(R.drawable.btn_roundconer);
                btn_fav.setTextColor(Color.parseColor("#ffffff"));

                btn_like.setBackgroundResource(R.drawable.btn_conerwhite);
                btn_like.setTextColor(Color.parseColor("#000000"));

                android.support.v4.app.FragmentTransaction fragmentTransaction;
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_like, new Favorites());
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        ImageView btn1 = (ImageView) findViewById(R.id.going_back);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView btn = (ImageView) findViewById(R.id.go_search);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(My_artis.this, Search.class);
                startActivity(variable);
                finish();
            }
        });
    }
}
