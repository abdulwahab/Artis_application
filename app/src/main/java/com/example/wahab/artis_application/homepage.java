package com.example.wahab.artis_application;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.wenchao.cardstack.CardAnimator;
import com.wenchao.cardstack.CardStack;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class homepage extends AppCompatActivity implements MoviesAdapter.onClickItem {
    private CardStack mCardStack;

    Toolbar toolbar;
    private List<Movies> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    private DrawerLayout mDrawerLayout;
    private String xyz;

    ImageView btn_menu;
    CardsDataAdapter mCardAdapter;
    List<Integer> drawableList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        ImageView btn = (ImageView) findViewById(R.id.go_search);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(homepage.this, Search.class);
                startActivity(variable);
                finish();
            }
        });

        final CardStack mCardStack = (CardStack) findViewById(R.id.container);

        mCardStack.setContentResource(R.layout.card_layout);
        mCardStack.setStackMargin(20);

        drawableList.add(R.drawable.img11);
        drawableList.add(R.drawable.homeimg);
        drawableList.add(R.drawable.likes);
        drawableList.add(R.drawable.likes1);
        drawableList.add(R.drawable.homeimg2);
        mCardAdapter = new CardsDataAdapter(this, drawableList);

        //set same image 5times
//        mCardAdapter.add("test1");
//        mCardAdapter.add("test2");
//        mCardAdapter.add("test3");
//        mCardAdapter.add("test4");
//        mCardAdapter.add("test5");
        mCardStack.setAdapter(mCardAdapter);

        ImageView reload = (ImageView) findViewById(R.id.reload_card);
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(homepage.this, "reload", Toast.LENGTH_SHORT).show();

                drawableList.add(R.drawable.img11);
                drawableList.add(R.drawable.homeimg);
                drawableList.add(R.drawable.homeimg2);
                drawableList.add(R.drawable.likes);
                drawableList.add(R.drawable.likes1);

                mCardAdapter = new CardsDataAdapter(homepage.this, drawableList);
                mCardStack.setAdapter(mCardAdapter);
            }
        });

        ImageView dlike = (ImageView) findViewById(R.id.dislike);
        dlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCardStack.discardTop(0);
            }
        });

        ImageView like = (ImageView) findViewById(R.id.like);
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCardStack.discardTop(1);

            }
        });


        if (mCardStack.getAdapter() != null) {
            Log.i("MyActivity", "Card Stack size: " + mCardStack.getAdapter().getCount());
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_side);

        mAdapter = new MoviesAdapter(movieList);
        mAdapter.setClick(homepage.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareMovieData();

        btn_menu = (ImageView) findViewById(R.id.menu);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

    }

    private void prepareMovieData() {
        Movies movie = new Movies("Search", R.drawable.ssearch);
        movieList.add(movie);

        movie = new Movies("My Artis", R.drawable.sheart);
        movieList.add(movie);

        movie = new Movies("Hot Artis", R.drawable.aag);
        movieList.add(movie);

        movie = new Movies("Upload", R.drawable.supload);
        movieList.add(movie);

        movie = new Movies("My Cart", R.drawable.cart);
        movieList.add(movie);

        movie = new Movies("Settings", R.drawable.gear);
        movieList.add(movie);

        movie = new Movies("Help", R.drawable.help);
        movieList.add(movie);

        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void itemClick(int position) {
        Log.d("itemClick: ", position + "" + movieList.get(position).getName());
        Toast.makeText(this, "" + movieList.get(position).getName(), Toast.LENGTH_SHORT).show();

        xyz = movieList.get(position).getName();

        if (xyz == "Search") {
            Intent cinemaIntent = new Intent(this, Search.class);
            startActivity(cinemaIntent);
        } else if (xyz == "Help") {
            Intent cinemaIntent = new Intent(this, Help.class);
            startActivity(cinemaIntent);
        } else if (xyz == "My Cart") {
            Intent cinemaIntent = new Intent(this, Shipping.class);
            startActivity(cinemaIntent);
        } else if (xyz == "My Artis") {
            Intent cinemaIntent = new Intent(this, My_artis.class);
            startActivity(cinemaIntent);
        } else if (xyz == "Settings") {
            Intent cinemaIntent = new Intent(this, Setting.class);
            startActivity(cinemaIntent);
        } else if (xyz == "Upload") {
            Intent cinemaIntent = new Intent(this, Upload_art.class);
            startActivity(cinemaIntent);
        } else if (xyz == "Hot Artis") {
            Intent cinemaIntent = new Intent(this, hometwo.class);
            startActivity(cinemaIntent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_side);
        drawer.closeDrawer(Gravity.LEFT);

    }
}
