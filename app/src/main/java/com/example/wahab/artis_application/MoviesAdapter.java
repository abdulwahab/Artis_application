package com.example.wahab.artis_application;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wahab.artis_application.Movies;
import com.example.wahab.artis_application.R;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<Movies> moviesList;
    private onClickItem callBack;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView name;
        public ImageView img;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            img = (ImageView) view.findViewById(R.id.img);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callBack.itemClick(getAdapterPosition());
        }

    }


    public MoviesAdapter(List<Movies> moviesList) {
        this.moviesList = moviesList;
    }

    public interface onClickItem {
        public void itemClick (int position);
    }
    public void setClick(onClickItem callBack){
        this.callBack = callBack;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_mobile, parent, false);

        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(MyViewHolder holder, int position) {
        Movies movie = moviesList.get(position);
        holder.name.setText(movie.getName());
        holder.img.setImageResource(movie.getImg());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}