package com.example.wahab.artis_application;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Wahab on 11/15/2017.
 */

public class ViewPagerAdapter extends PagerAdapter {
    private int[] image_resource = {R.drawable.seen, R.drawable.likes1, R.drawable.likes};
    private Context ctx;
    private LayoutInflater layoutInflater;

    public ViewPagerAdapter(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return image_resource.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout) object);
    }
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = layoutInflater.inflate(R.layout.swipe_layout, container, false);
        final ImageView imageView = (ImageView) item_view.findViewById(R.id.image_view);

        imageView.setBackgroundResource(image_resource[position]);
        container.addView(item_view);
        PhotoViewAttacher photoView = new PhotoViewAttacher(imageView);
        photoView.update();

        return item_view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((LinearLayout) object);
    }
}
