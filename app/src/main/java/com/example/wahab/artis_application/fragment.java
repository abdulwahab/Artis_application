package com.example.wahab.artis_application;

/**
 * Created by Wahab on 11/13/2017.
 */

public class fragment {

    private int img;
    private int side_img;

    public fragment(int img, int side_img) {
        this.img = img;
        this.side_img = side_img;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getSide_img() {
        return side_img;
    }

    public void setSide_img(int side_img) {
        this.side_img = side_img;
    }
}
