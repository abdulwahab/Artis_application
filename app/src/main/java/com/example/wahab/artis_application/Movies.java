package com.example.wahab.artis_application;

import android.media.Image;

public class Movies {

    private String name;
    private int img;

    public Movies(String name, int img) {
        this.name = name;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

}
