package com.example.wahab.artis_application;

import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

/**
 * Created by Wahab on 11/13/2017.
 */

public class FragmentAdapter extends RecyclerView.Adapter<FragmentAdapter.MyViewHolder> {

    private List<fragment> cardList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout img;
        public  ImageView side_img;

        public MyViewHolder(View view) {
            super(view);
            img =  view.findViewById(R.id.bg_img);
            side_img = (ImageView) view.findViewById(R.id.s_img);
        }
    }

    public FragmentAdapter(List<fragment> cardList){
        this.cardList = cardList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_list, parent, false);

        //screen divided code
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        lp.height = parent.getMeasuredHeight() / 3;
        itemView.setLayoutParams(lp);

        return new FragmentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        fragment frag = cardList.get(position);
        holder.img.setBackgroundResource(frag.getImg());
        holder.side_img.setImageResource(frag.getSide_img());
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }


}
