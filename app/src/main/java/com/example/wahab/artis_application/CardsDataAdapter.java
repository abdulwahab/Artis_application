package com.example.wahab.artis_application;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CardsDataAdapter extends ArrayAdapter<String> {

    List<Integer> listDrawable = new ArrayList<>();

    @Override
    public int getCount() {
        return listDrawable.size();
    }

    public CardsDataAdapter(Context context, List<Integer> temp) {
        super(context, R.layout.card_layout);
        listDrawable.clear();
        listDrawable.addAll(temp);

        //only set one image
        //TextView tinder_img = (TextView) view.findViewById(R.id.content);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, final View contentView, ViewGroup parent) {
        TextView v = (TextView) (contentView.findViewById(R.id.content));
//        v.setText(getItem(position));
        v.setBackground(contentView.getContext().getDrawable(listDrawable.get(position)));
        return contentView;
    }
}

