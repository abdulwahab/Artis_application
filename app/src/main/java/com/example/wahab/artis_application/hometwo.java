package com.example.wahab.artis_application;

import android.content.Intent;
import android.graphics.Movie;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class hometwo extends AppCompatActivity implements MoviesAdapter.onClickItem {
    private List<Movies> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    private DrawerLayout mDrawerLayout;
    private  String xyz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hometwo);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_new);

        mAdapter = new MoviesAdapter(movieList);
        mAdapter.setClick(hometwo.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareMovieData();

        ImageView btn_menu = (ImageView) findViewById(R.id.side_menu);
        btn_menu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        ImageView btn = (ImageView) findViewById(R.id.go_search);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(hometwo.this, Search.class);
                startActivity(variable);
                finish();
            }
        });
    }

    private void prepareMovieData() {
        Movies movie = new Movies("Search", R.drawable.ssearch);
        movieList.add(movie);

        movie = new Movies("My Artis", R.drawable.sheart);
        movieList.add(movie);

        movie = new Movies("Hot Artis", R.drawable.aag);
        movieList.add(movie);

        movie = new Movies("Upload", R.drawable.supload);
        movieList.add(movie);

        movie = new Movies("My Cart", R.drawable.cart);
        movieList.add(movie);

        movie = new Movies("Settings", R.drawable.gear);
        movieList.add(movie);

        movie = new Movies("Help", R.drawable.help);
        movieList.add(movie);

        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void itemClick(int position) {
        Log.d( "itemClick: ",position+""+movieList.get(position).getName());
        Toast.makeText(this, ""+movieList.get(position).getName(), Toast.LENGTH_SHORT).show();

        xyz = movieList.get(position).getName();

        if ( xyz == "Search" ) {
            Intent cinemaIntent = new Intent(this, Search.class);
            startActivity(cinemaIntent);
        }
         else if (xyz == "Help") {
            Intent cinemaIntent = new Intent(this, Help.class);
            startActivity(cinemaIntent);
        }

        else if (xyz == "My Cart") {
            Intent cinemaIntent = new Intent(this, Shipping.class);
            startActivity(cinemaIntent);
        }

        else if (xyz == "My Artis") {
            Intent cinemaIntent = new Intent(this, My_artis.class);
            startActivity(cinemaIntent);
        }

        else if (xyz == "Settings") {
            Intent cinemaIntent = new Intent(this, Setting.class);
            startActivity(cinemaIntent);
        }
        else if (xyz == "Upload") {
            Intent cinemaIntent = new Intent(this, Upload_art.class);
            startActivity(cinemaIntent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_new);
        drawer.closeDrawer(Gravity.LEFT);

    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
