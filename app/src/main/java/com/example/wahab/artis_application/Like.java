package com.example.wahab.artis_application;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class Like extends Fragment {

    private List<fragment> cardList = new ArrayList<>();
    private RecyclerView recyclerView;
    private FragmentAdapter fAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_like, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_like_fragment);

        fAdapter = new FragmentAdapter(cardList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(fAdapter);

        prepareFragmentData();
    return view;
    }

    private void prepareFragmentData() {
        fragment list = new fragment(R.drawable.likes, R.drawable.mylikes);
        cardList.add(list);

        list = new fragment(R.drawable.likes1, R.drawable.mylikes);
        cardList.add(list);

        list = new fragment(R.drawable.homeimg, R.drawable.mylikes);
        cardList.add(list);

        list = new fragment(R.drawable.likes, R.drawable.mylikes);
        cardList.add(list);

        list = new fragment(R.drawable.likes1, R.drawable.mylikes);
        cardList.add(list);

        list = new fragment(R.drawable.homeimg, R.drawable.mylikes);
        cardList.add(list);

        list = new fragment(R.drawable.likes, R.drawable.mylikes);
        cardList.add(list);

        list = new fragment(R.drawable.likes1, R.drawable.mylikes);
        cardList.add(list);


        fAdapter.notifyDataSetChanged();
    }

}
